module.exports = {

	STAGE: true,
	PORT: 80,
	API_URL: '/wp-json/wp/v2',
	API_ORIGIN: '',
	API_PATH: '/wp-json/wp/v2',
	API_MENU_PATH: '/wp-json/wp-api-menus/v2'

};