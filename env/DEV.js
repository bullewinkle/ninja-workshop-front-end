module.exports = {

	DEV: true,
	API_URL: 'http://nws.local/wp-json/wp/v2',
	API_ORIGIN: 'http://nws.local',
	API_PATH: '/wp-json/wp/v2',
	API_MENU_PATH: '/wp-json/wp-api-menus/v2'

};