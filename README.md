[![Build Status](https://travis-ci.org/iboozyvoozy/starter.svg)](https://travis-ci.org/iboozyvoozy/starter) [![Dependency Status](https://gemnasium.com/iboozyvoozy/starter.svg)](https://gemnasium.com/iboozyvoozy/starter)

# Starting kit for angular application


## Global dependencies

`node`, `npm`

`gettext`, `msgmerge`, `msgcmp`


## Install

`npm i -g bower`

`npm i `


## Start

`npm run {dev|stage|prod}`


## i18n

`gulp i18n:extract`

`gulp i18n:merge`

`gulp i18n:check`

`gulp i18n:update`

`gulp i18n:test`


## Test

`npm run test-e2e`

`npm run test-e2e-watch` - for auto run a changed e2e-test

`npm run test-unit`

`npm run test-i18n`

