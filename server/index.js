// Dependencies
var express = require('express');
var path = require('path');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var httpProxy = require('http-proxy');

// Configuration
var assets = path.resolve(__dirname, '../dist');
var index = path.resolve(__dirname, '../dist/index.html');
var pallete = path.resolve(__dirname, '../dist/pallete.html');
var PORT = process.env.PORT || 9000;
var BASE_ROUTE = process.env.BASE_ROUTE || '';

// Express application
var app = express();

// Proxy server (don't verify SSL certificate)
var apiProxy = httpProxy.createProxyServer({secure: false});

// Middlewares
app.use(compression());

if (BASE_ROUTE) {
	app.use(baseRouteRewrite(BASE_ROUTE));
}

app.use('/assets', express.static(assets));
app.use(methodOverride());

// proxy api
app.all('/api/*', function (req, res) {
	var host = 'support.selectel.ru';

	apiProxy.web(req, res, {
		target: 'https://' + host,
		headers: {
			host: host,
			cookie: 'uid=733; sid=796741be05c4b9476218da2835306778'
		}
	});
});

// use middleware for proxy before using the bodyparser
// https://github.com/nodejitsu/node-http-proxy/issues/180
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

// Stupid serve SPA
app.all('/pallete', function (req, res) {res.sendFile(pallete)});
app.all('/*', function (req, res) {res.sendFile(index)});


// Start server
app.listen(PORT, function (err) {
	if (err) throw err;
	console.log('[app] started on PORT: %s', PORT);
});

// rewrite rule for base route
function baseRouteRewrite(baseRoute) {
	return function (req, res, next) {
		var reg = new RegExp(`${baseRoute}(.*?\\.(css|js|html|map))`),
			urlMatch = reg.exec(req.url);

		if (urlMatch) {
			req.url = `/${urlMatch[1]}`;
		}

		next();
	}
}