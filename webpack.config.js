// Dependencies
var path = require('path');
var wp = require('webpack');


// Main paths
var __ENV = path.resolve(__dirname, 'env');
var __SRC = path.resolve(__dirname, 'src');
var __DIST = path.resolve(__dirname, 'dist');
var __I18N = path.resolve(__dirname, 'i18n');
var __BOWER = path.resolve(__dirname, 'bower_components');

// Environment
var ENV = process.env.ENV;
var CFG = ENV ? require(path.resolve(__ENV, ENV)) : {};
var DEV = ENV === 'DEV';

var $ = {
  HTML: require('html-webpack-plugin'),
  Cleaner: require('clean-webpack-plugin'),
  Extracter: require('extract-text-webpack-plugin'),
  Definer: wp.DefinePlugin,
  Provider: wp.ProvidePlugin,
  ContextReplacementPlugin: wp.ContextReplacementPlugin,
  NgAnnotate: require('ng-annotate-webpack-plugin')
};

var bootstrapStylPath = 'node_modules/bootstrap-styl';
var minimiseStyl = DEV ? '-minimize' : 'minimize';

// Add trailing slash for BASE_ROUTE
if (CFG.BASE_ROUTE) {
  if (CFG.BASE_ROUTE.slice(-1) !== '/') {
    CFG.BASE_ROUTE += '/';
  }
} else {
  CFG.BASE_ROUTE = '/';
}

// Add trailing slash for API_COMMON
if (CFG.API_COMMON) {
  if (CFG.API_COMMON.slice(-1) !== '/') {
    CFG.API_COMMON += '/';
  }
}

//// Add trailing slash for API_URL
//if (CFG.API_URL) {
//	if (CFG.API_URL.slice(-1) !== '/') {
//		CFG.API_URL += '/';
//	}
//}

// Extend process.env
for (var prop in CFG) {
  process.env[prop] = CFG[prop];
}

// Webpack configuration object
// http://webpack.github.io/docs/configuration.html
var cfg = {

  context: __SRC,

  entry: {
    vendor: 'vendor/vendor.module',
    app: 'app/app.module'
  },

  output: {
    path: path.resolve(__DIST, ''),
    publicPath: '/assets/',
    filename: DEV ? '[name].js' : '[name]-[chunkhash].js',
  },

  resolve: {
    modules: [__SRC, 'node_modules'],
    extensions: ['.js', '.coffee', '.json', '.styl', '.jade', '.pug'],
    alias: {
      font: path.resolve(__SRC, 'assets/font'),
      img: path.resolve(__SRC, 'assets/img'),
      i18n: path.resolve(__I18N),
    }
  },

  module: {
    noParse: [
      /node_modules\/angular\/angular\.js/,
      /node_modules\/angular-animate\/angular-animate\.js/,
      /node_modules\/angular-ui-router/,
      /node_modules\/angular-ui-bootstrap\/ui-bootstrap-tpls/,
      /node_modules\/lodash/
    ],


    rules: [

      {
        test: /\.js$/,
        include: [__SRC],
        use: [{
          loader: 'ng-annotate-loader',
          options: {es6: true}
        }, {
          loader: "babel-loader",
          options: {
            presets: [["es2015"]]
          }
        }]
      },
      {
        test: /\.coffee$/,

        include: [__SRC],
        use: [{
          loader: 'ng-annotate-loader',
          options: {es6: true}
        }, {
          loader: "coffee-loader",
          options: {
            bare: true
          }
        }
        ]
      },
      {
        test: /\.css$/,
        use: DEV
          ?[
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
            options: {sourceMap: true, importLoaders: 1}
          },
          {
            loader: `postcss-loader`,
            options: {sourceMap: true}
          }
        ]
          : $.Extracter.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: "css-loader",
                options: {sourceMap: true, importLoaders: 1}
              },
              {
                loader: `postcss-loader`,
                options: {sourceMap: true}
              }
            ]
          }),
      },
      {
        test: /\.styl$/,
        use: DEV
          ? [
            {
              loader: `style-loader`,
            },
            {
              loader: `css-loader`,
              options: {sourceMap: true, importLoaders: 2}
            },
            {
              loader: `postcss-loader`,
              options: {sourceMap: true}
            },
            {
              loader: `stylus-loader`,
              options: {
                sourceMap: true,
                paths: ['node_modules', bootstrapStylPath]
              }
            }
          ]
          : $.Extracter.extract({
            fallback: 'style-loader',
            use: [
              {
                loader: `css-loader`,
                options: {sourceMap: true, importLoaders: 2}
              },
              {
                loader: `postcss-loader`,
                options: {sourceMap: true}
              },
              {
                loader: `stylus-loader`,
                options: {
                  sourceMap: true,
                  paths: ['node_modules', bootstrapStylPath]
                }
              }
            ]
          }),
      },
      {
        test: /index.jade$/,
        use: [{
          loader: `jade-loader`,
          options: {
            pretty: DEV
          },
        }],
      },
      {
        test: /pallete.jade$/,
        use: [{
          loader: `jade-loader`,
          options: {
            pretty: DEV
          },
        }]
      },
      {
        test: /\.tpl\.jade$/,
        use: [{
          loader: 'ng-cache-loader',
        }, {
          loader: 'jade-html-loader',
        }],
      },
      {
        test: /\.json$/,
        use: [{
          loader: 'json-loader',
        }],
      },
      {
        test: /\.(svg|png|jpg|eot|ttf|woff|woff2|pdf)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: '[name]-[hash].[ext]'
          },
        }],
      }

    ]
  },

  cache: DEV,

  devtool: DEV ? 'eval' : undefined,

  plugins: [
    new $.Extracter({
      filename: ('[name]-[contenthash].css'),
      disable: false,
      allChunks: true
    }),
    new $.HTML({
      inject: true,
      minify: false,
      filename: 'index.html',
      template: path.resolve(__SRC, 'index.jade'),
      chunksSortMode: sortChunks
    }),
    new $.HTML({
      inject: true,
      minify: false,
      filename: 'pallete.html',
      template: path.resolve(__SRC, 'pallete.jade'),
      chunksSortMode: sortChunks
    }),
    new $.Definer({ENV: JSON.stringify(CFG)}),
    new $.Provider({}),
    new $.ContextReplacementPlugin(
      /node_modules\/moment\/locale/,
      /ru|en-gb/
    ),
    new wp.LoaderOptionsPlugin({
      debug: true
    }),
  ],

  devServer: {
    host: '0.0.0.0',
    port: '9009',
    historyApiFallback: true
  }
};

// Clean distribution
// cfg.plugins.push(
// 	new $.Cleaner(['dist'])
// );

// Export configuration
module.exports = cfg;


function sortChunks(a, b) {  //order like in cfg
  let entries = Object.keys(cfg.entry);
  let indexA = entries.indexOf(a.names[0]);
  let indexB = entries.indexOf(b.names[0]);
  if (indexA > indexB) {
    return 1;
  }
  if (indexA < indexB) {
    return -1;
  }
  return 0;
};