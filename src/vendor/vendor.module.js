// Vendor styles
// require('./vendor.styl')

// JS vendors
window.$ = require('jquery');
require('slick-carousel');

window._ = require('lodash');
window.moment = require('moment/moment');

// Native angular vendors
require('angular');
require('angular-i18n/angular-locale_ru-ru.js');
require('angular-aria');
require('angular-touch');
require('angular-animate');
require('angular-cookies');
require('angular-resource');
require('angular-sanitize');
require('angular-messages');
require('angular-cache');

// 3rd party angular vendors
require('angular-ui-router/release/angular-ui-router');
require('angular-ui-bootstrap');
require('angular-ui-notification');
require('angular-carousel');
require('ngstorage');

// Main vendor module
angular.module('vendor', [
	'ngAria',
	'ngTouch',
	'ngAnimate',
	'ngCookies',
	'ngResource',
	'ngSanitize',
	'ngMessages',
	'ngStorage',
	'ui.router',
	'ui.bootstrap',
	'angular-cache',
	'ui-notification',
	'angular-carousel',
]);


// Fastclick initialization
angular
	.module('vendor')
	.config( ($localStorageProvider) => {
		"ngInject";
		$localStorageProvider.setKeyPrefix(`ninja's workshop: `)
	})
	.run( () => {
		var FastClick = require('fastclick/lib/fastclick');
		FastClick.attach(document.body);
	});