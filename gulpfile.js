console.log('__dirname');
console.log(__dirname);

// Dependencies
var g = require('gulp');
var $ = require('gulp-load-plugins')();
var watch = require('gulp-watch');
var wp = require('webpack');
var cfg = require('./webpack.conf');
var seq = require('run-sequence');
var browserSync = require('browser-sync');
var spawn = require('child_process').spawn;
var PORT = process.env.PORT || 9000;

// Running childProcesses
var childProcess = {};

// Webpack watch build
g.task('wp:watch', function (done) {
	// Flag for handling first build
	// 'done()' callback must be called only once
	var builded = false;
	wp(cfg).watch(300, function (err, stats) {
		if (err) throw new $.util.PluginError('webpack', err);
		console.log('[webpack]', stats.toString({colors: true}));
		if (builded) return;
		builded = true;
		done();
	});
});


// Webpack simple build
g.task('wp:full', function (done) {
	wp(cfg).run(function (err, stats) {
		if (err) throw new $.util.PluginError('webpack', err);
		$.util.log('[webpack]', stats.toString({colors: true}));
		done();
	});
});


// Webpack developement build
// TODO: understand howto:
// - trigger reload for extracted styles
// - proxy API server on 9000 port
// - historyApiFallback for SPA apps
g.task('wp:dev', function () {
	var DevServer = require('webpack-dev-server');
	var compiler = wp(cfg);
	var port = PORT + 1;
	var server = new DevServer(compiler, {
		hot: false,
		stats: {colors: true},
		historyApiFallback: true
	});
	server.listen(port, function (err) {
		if (err) throw new $.util.PluginError('webpack', err);
	});
});


// Production minification for js/css
// Grab all scripts and styles and compress with
// 'gulp-uglify' and 'gulp-minify-css'
g.task('min', function () {
	var filterJS = $.filter(['**/*.js'], {restore: true});
	var filterCSS = $.filter(['**/*.css'], {restore: true});
	var optionsJS = {
		mangle: true
	};
	var optionsCSS = {
		advanced: false,
		aggressiveMerging: false
	};
	return g.src('dist/**/*', {base: 'dist'})
		.pipe(filterJS)
		.pipe($.uglify(optionsJS))
		.pipe(filterJS.restore)
		.pipe(filterCSS)
		.pipe($.minifyCss(optionsCSS))
		.pipe(filterCSS.restore)
		.pipe(g.dest('dist'));
});


// Browsersync server
g.task('sync', function (done) {
	require('./server');

	browserSync({
		port: +PORT + 1,
		files: 'dist/**/*',
		proxy: 'localhost:' + PORT,
		ghostMode: {
			forms: true,
			clicks: true,
			scroll: true,
			location: true
		},
		browser: [],
		notify: false,
		scrollRestoreTechnique: 'cookie',
		scriptPath: function (path, port, options) {
			return options.get("absolute");
		}
	}, done);
});


// Main tasks
g.task('build:dev', function (done) {
	seq('wp:watch', 'sync', done);
});

g.task('build:fast', function (done) {
	seq('wp:full', done);
});

g.task('build:full', function (done) {
	seq('wp:full', 'min', done);
});

g.task('build:test-e2e', function (done) {
	seq(
		'run-webdriver',
		'wp:full',
		'run-sync-for-e2e',
		'run-protractor',
		done
	);
});

g.task('build:test-e2e-watch', function(done) {
	seq(
		'run-webdriver',
		'wp:full',
		'run-sync-for-e2e',
		'test-e2e-watch',
		done
	);
});

function spawnProtractor(args) {
	args = args || [];

	return spawn('./node_modules/.bin/protractor', ['./test/protractor.conf'].concat(args), {
		stdio: 'inherit',
		env: process.env
	})
}